package solution

import java.util.regex.Pattern

object OrdersSummary {
    private val statementsDelimiter = Pattern.compile(",(\\s)*")
    private val statementDelimiter = Pattern.compile("(\\s)+")

    fun balanceStatements(lst: String): String {
        return if(!lst.isEmpty()) {
            var sell: Long = 0
            var buy: Long = 0
            val errors: MutableList<String> = ArrayList()
            lst.split(statementsDelimiter).forEach { s ->
                val statement = parseStatement(s)
                if (statement.first) {
                    if (statement.second > 0)
                        sell += statement.second
                    else
                        buy += statement.second * -1
                } else {
                    errors.add(s)
                }
            }
            val bad = if (errors.size > 0) "; Badly formed ${errors.size}: ${errors.joinToString(" ;")} ;" else ""
            "Buy: $buy Sell: $sell$bad"
        } else {
            "Buy: 0 Sell: 0"
        }
    }

    private fun parseStatement(s: String): Pair<Boolean, Long> {
        val statement = s.split(statementDelimiter)
        return if (statement.size == 4) {
            try {
                val q = statement[1].toLong().toBigDecimal()
                if(!statement[2].contains(".")) throw java.lang.Exception()
                val p = statement[2].toBigDecimal()
                when (statement[3]) {
                    "B" -> Pair(true, (q.multiply(p)).toLong() * -1)
                    "S" -> Pair(true, (q.multiply(p)).toLong())
                    else -> throw java.lang.Exception()
                }
            } catch (e: Exception) {
                Pair(false, 0L)
            }
        } else Pair(false, 0L)
    }
}

fun main(args: Array<String>) {
    println(OrdersSummary.balanceStatements("GOOG 300 542.0 B, AAPL 50 145.0 B, CSCO 250.0 29 B, GOOG 200 580.0 S"))
    println(OrdersSummary.balanceStatements("GOOG 300 542.93 B, CLH15.NYM 50 56.32 S, CSCO 250 29.46 B, OGG 20 580.1 B"))
    println(OrdersSummary.balanceStatements("ZNGA 1300 2.66 S, CLH15.NYM 50 56.32 S, OWW 1000 11.623 S, OGG 20 580.1 S"))
}
